.TH %%CTOOLNAME%% 1 "November 2015" CODONW "User Commands"
.SH NAME
%%TOOLNAME%% \- %%TOOLDESC%%
.SH SYNOPSIS
.B %%TOOLNAME%%
.I inputfile
.I outputfile
.I bulkoutputfile
<options>
.SH DESCRIPTION
.B %%TOOLNAME%%
is a shortcut to accessing the
.BR codonw (1)
executable, with the '%%TOOLDESC%%' option (-%%TOOLOPT%%) preselected, i.e.
.TP
codonw -%%TOOLOPT%% ...
.PP
The command line arguments follow the order of arguments in
.BR codonw
itself.
.SH "SEE ALSO"
.BR codonw (1)