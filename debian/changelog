codonw (1.4.4-6) unstable; urgency=medium

  * Drop make call in d/rules (Closes: #982989)
    Thanks to Nilesh Patra for the  patch.
  * Switch to maintainer manpage and drop b-d on help2man.
  * Bump Standards-Version.
  * Remove obsolete lintian override.
  * Update watchfile version.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 20 Feb 2021 10:24:36 +0100

codonw (1.4.4-5) unstable; urgency=medium

  * Team Upload.

  [ Jelmer Vernooĳ ]
  * Move source package lintian overrides to debian/source.

  [ Nilesh Patra ]
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Add salsa-ci file (routine-update)
  * routine-update: Add salsa-ci file
  * Add "Rules-Requires-Root:no"
  * Add autopkgtest.

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 15 May 2020 22:47:57 +0530

codonw (1.4.4-4) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - Added ref to OMICtools
    - Added associated paper
  * Corrected upstream name to codonW

  [ Dylan Aïssi ]
  * Fix OMICS ID

  [ Sascha Steinbiss ]
  * Use secure d/copyright format link.
  * Use debhelper 11.
  * Bump Standards-Version.
  * Remove unnecessary patch file.
  * Update Uploader email address.
  * Update Vcs-* fields with Salsa addresses.
  * Fix whitespace issue in changelog.
  * Use recent Lintian override names.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 04 Jul 2018 18:52:50 +0200

codonw (1.4.4-3) unstable; urgency=low

  * Fix typo in d/copyright
  * Add EDAM annotation
  * Bump Standards-Version.
  * Use secure Vcs-Git.
  * Add Lintian overrides for pedantic issues.
  * Fix spelling.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Fri, 26 Feb 2016 08:48:08 +0000

codonw (1.4.4-2) unstable; urgency=medium

  * Remove nondeterminism
    - menu.c: Do not print compilation date and time

 -- Sascha Steinbiss <sascha@steinbiss.name>  Mon, 07 Dec 2015 22:51:58 +0000

codonw (1.4.4-1) unstable; urgency=low

  * Initial release (Closes: #805693)

 -- Sascha Steinbiss <sascha@steinbiss.name>  Fri, 20 Nov 2015 21:24:47 +0000
